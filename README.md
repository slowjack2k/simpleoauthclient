# A small javafx sample to get an OAUTH2 access token

This sample uses a javafx webview to retrieve an OAUTH2 access token from auth0.com.
For this example the "Authorization Code Flow" is used. Custom schemes, as callbacks, 
are supported.

```bash
> mvn package
> mkdir target/config
> cp config/default.properties.tmpl target/config/default.properties
> cd target
>  
> java -jar simple-oauth-client-0.0.1-SNAPSHOT.jar
```