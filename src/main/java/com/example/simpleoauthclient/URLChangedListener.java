package com.example.simpleoauthclient;

import java.net.URL;

public interface URLChangedListener {

	void urlChanged(URL oldURL, URL newURL);

}