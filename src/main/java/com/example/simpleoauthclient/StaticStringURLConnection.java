package com.example.simpleoauthclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.codec.Charsets;

public class StaticStringURLConnection extends URLConnection {

	protected StaticStringURLConnection(URL url) {
		super(url);
	}

	private byte[] data = "<html><body>DONE</body></html>".getBytes(Charsets.UTF_8);

	@Override
	public void connect() throws IOException {
		if (connected) {
			return;
		}

		connected = true;
	}

	@Override
	public String getHeaderField(String name) {
		if ("Content-Type".equalsIgnoreCase(name)) {
			return getContentType();
		} else if ("Content-Length".equalsIgnoreCase(name)) {
			return "" + getContentLength();
		}
		return null;
	}

	@Override
	public String getContentType() {

		return "text/html; charset=utf-8";
	}

	@Override
	public int getContentLength() {
		return data.length;
	}

	@Override
	public long getContentLengthLong() {
		return data.length;
	}

	@Override
	public boolean getDoInput() {
		return true;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		connect();
		return new ByteArrayInputStream(data);
	}

	@Override
	public OutputStream getOutputStream() throws IOException {

		return new ByteArrayOutputStream();
	}

	@Override
	public java.security.Permission getPermission() throws IOException {
		return null;
	}

}