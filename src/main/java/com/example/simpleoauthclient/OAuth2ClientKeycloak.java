package com.example.simpleoauthclient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OAuth2ClientKeycloak implements OAuth2Client {
	private Logging logger;
	private static final String CODEVERIFIER = "_AAEbXQvzdT5sfskzkCB0HPTi0QaxwuhD2wv1DL-347cw-s4";
	private static final String CODECHALLENGE = Base64.getUrlEncoder().withoutPadding()
			.encodeToString(DigestUtils.sha256(CODEVERIFIER)); // 7mRlDihiz7XHJguFrH59eklG3ZCt2Uhm5au7kTCtKzk

	public OAuth2ClientKeycloak(Logging logger) {
		super();
		this.logger = logger;
	}

	@Override
	public URIBuilder createAuthURI(OAuth2ClientData data) {
		URIBuilder builder = new URIBuilder();
		try {

			builder = new URIBuilder(data.getHost()).setPath(data.getAuthEndpoint())//
					.addParameter("response_type", "code")//
					.addParameter("client_id", data.getClientId()) //
					.addParameter("code_challenge_method", "S256") //
					.addParameter("code_challenge", CODECHALLENGE) //
					.addParameter("redirect_uri", data.getRedirectURI())//
					.addParameter("scope", data.getScope())//
					.addParameter("state", UUID.randomUUID().toString())//
					.addParameter("prompt", "login");

		} catch (URISyntaxException ex) {
			logger.logException(ex);
		}

		logger.logInfo(builder.toString());

		return builder;
	}

	private URIBuilder createTokenURI(OAuth2ClientData data) {
		URIBuilder builder = new URIBuilder();
		try {
			builder = new URIBuilder(data.getHost()).setPath(data.getTokenEndpointTxt());
			/*
			 * .addParameter("response_type", "code")// .addParameter("client_id",
			 * clientIdTxt.getText()).addParameter("audience", audienceTxt.getText())//
			 * .addParameter("redirect_uri", redirectURITxt.getText())//
			 * .addParameter("scope", "profile")// .addParameter("state",
			 * "FvF6uikg9ah5dmvns")// .addParameter("prompt", "login");
			 */
		} catch (URISyntaxException ex) {
			logger.logException(ex);
		}

		return builder;
	}

	@Override
	public JWTAccessToken exchangeCodeForToken(OAuth2ClientData data, String code) {

		CloseableHttpClient client = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost(createTokenURI(data).toString());

		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("grant_type", "authorization_code"));
		params.add(new BasicNameValuePair("client_id", data.getClientId()));
		params.add(new BasicNameValuePair("code_verifier", CODEVERIFIER));
		params.add(new BasicNameValuePair("redirect_uri", data.getRedirectURI()));
		params.add(new BasicNameValuePair("code", code));

		try {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
			logEntity(entity);

			httpPost.setEntity(entity);

			CloseableHttpResponse response = client.execute(httpPost);
			String jsonInput = EntityUtils.toString(response.getEntity(), "UTF-8");

			ObjectMapper mapper = new ObjectMapper();
			TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
			};
			Map<String, String> map = mapper.readValue(jsonInput, typeRef);

			JWTAccessToken token = new JWTAccessToken(map.get("access_token"));

			logger.logInfo("Access-Token: " + map.get("access_token"));

			return token;

		} catch (IOException e) {
			logger.logException(e);
		}

		return new JWTAccessToken(" . . ");
	}

	@Override
	public String toString() {
		return "Keycloak / PKCE";
	}

	private void logEntity(UrlEncodedFormEntity entity) throws IOException, UnsupportedEncodingException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		entity.writeTo(baos);

		logger.logInfo("Post-Entity: " + baos.toString("UTF-8"));
	}
}
