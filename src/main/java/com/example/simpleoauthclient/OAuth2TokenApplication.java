package com.example.simpleoauthclient;

import java.net.URL;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class OAuth2TokenApplication extends Application {
	private Scene scene;

	@Override
	public void start(Stage stage) throws Exception {
		try {
			StaticStringURLStreamHandlerFactory streamFactory = new StaticStringURLStreamHandlerFactory();
			URL.setURLStreamHandlerFactory(streamFactory);

			stage.setTitle("Web View");

			scene = new Scene(new FormBuilder(streamFactory).create(), 1080, 1080, Color.web("#666970"));
			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		launch(args);
	}

}
