package com.example.simpleoauthclient;

public interface Logging {

	void logError(String msg);

	void logInfo(String msg);

	void logException(Exception ex);

}