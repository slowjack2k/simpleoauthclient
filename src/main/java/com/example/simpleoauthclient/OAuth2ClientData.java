package com.example.simpleoauthclient;

public class OAuth2ClientData {
	private String clientId;
	private String clientSecret;
	private String redirectURI;
	private String host;
	private String audience;
	private String authEndpoint;
	private String tokenEndpointTxt;
	private String scope;

	public OAuth2ClientData(String clientId, String clientSecret, String redirectURI, String host, String audience,
			String authEndpoint, String tokenEndpointTxt, String scope) {
		super();
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.redirectURI = redirectURI;
		this.host = host;
		this.audience = audience;
		this.authEndpoint = authEndpoint;
		this.tokenEndpointTxt = tokenEndpointTxt;
		this.scope = scope;
	}

	public String getClientId() {
		return clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public String getRedirectURI() {
		return redirectURI;
	}

	public String getHost() {
		return host;
	}

	public String getAudience() {
		return audience;
	}

	public String getAuthEndpoint() {
		return authEndpoint;
	}

	public String getTokenEndpointTxt() {
		return tokenEndpointTxt;
	}

	public String getScope() {
		return scope;
	}

}
