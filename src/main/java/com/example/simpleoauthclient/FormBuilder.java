package com.example.simpleoauthclient;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;

import com.sun.javafx.webkit.WebConsoleListener;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class FormBuilder implements Logging, URLChangedListener {
	private static final DefaultValues DEFAULT_VALUES = new DefaultValues();

	private TextField clientIdTxt;
	private TextField clientSecretTxt;
	private TextField redirectURITxt;
	private TextField hostTxt;
	private TextField audienceTxt;
	private TextField authEndpointTxt;
	private TextField tokenEndpointTxt;
	private TextField scopeTxt;
	private TextField codeTxt;
	private ChoiceBox<OAuth2Client> clientChoiceBox;

	private TextArea loggingArea;

	private StaticStringURLStreamHandlerFactory urlStreamHandlerFactory;

	private WebEngine webEngine;

	private OAuth2Client oAuth2Client;

	public FormBuilder(StaticStringURLStreamHandlerFactory urlStreamHandlerFactory) {
		super();
		this.urlStreamHandlerFactory = urlStreamHandlerFactory;
	}

	public GridPane create() {
		GridPane root = new GridPane();
		GridPane form = new GridPane();
		WebView browser = new WebView();
		webEngine = browser.getEngine();

		webEngine.locationProperty().addListener(locationChangedListener());
		webEngine.setJavaScriptEnabled(true);

		webEngine.getLoadWorker().exceptionProperty().addListener(new ChangeListener<Throwable>() {
			@Override
			public void changed(ObservableValue<? extends Throwable> ov, Throwable t, Throwable t1) {
				logError("Received exception: " + t1.getMessage());
			}
		});

		WebConsoleListener.setDefaultListener(new WebConsoleListener() {
			@Override
			public void messageAdded(WebView webView, String message, int lineNumber, String sourceId) {
				logError("Console: [" + sourceId + ":" + lineNumber + "] " + message);

			}
		});

		Text clientIdLabel = new Text("client id");
		Text clientSecretLabel = new Text("client secret");
		Text redirectURILabel = new Text("redirect URI");
		Text hostLabel = new Text("host");
		Text audienceLabel = new Text("audience");
		Text authEndpointLabel = new Text("Auth-Endpoint");
		Text tokenEndpointLabel = new Text("Token-Endpoint");
		Text scopeLabel = new Text("Scope");
		Text codeLabel = new Text("Code (optional)");
		Text clientChooserLabel = new Text("Provider/Flow");

		clientIdTxt = new TextField(DEFAULT_VALUES.getClientId());
		clientSecretTxt = new TextField(DEFAULT_VALUES.getClientSecret());
		redirectURITxt = new TextField(DEFAULT_VALUES.getRedirectURI());
		hostTxt = new TextField(DEFAULT_VALUES.getHost());
		audienceTxt = new TextField(DEFAULT_VALUES.getAudience());
		authEndpointTxt = new TextField(DEFAULT_VALUES.getAuthEndpoint());
		tokenEndpointTxt = new TextField(DEFAULT_VALUES.getTokenEndpoint());
		scopeTxt = new TextField(DEFAULT_VALUES.getScope());
		codeTxt = new TextField();
		clientChoiceBox = new ChoiceBox<>();

		loggingArea = new TextArea();

		Button submit = new Button("Start Authorization Code Flow");
		Button exchangeCode = new Button("Exchange code for token");

		clientChoiceBox.setItems(
				FXCollections.observableArrayList(new OAuth2ClientKeycloak(this), new OAuth2ClientAuth0(this)));

		clientChoiceBox.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> oAuth2Client = newValue);

		clientChoiceBox.getSelectionModel().selectFirst();

		submit.setOnAction(callAuthorize());
		exchangeCode.setOnAction(callExchange());

		loggingArea.setMinSize(150, 200);

		root.setMinSize(750, 1024);
		form.setMinSize(500, 250);

		// Setting the padding
		root.setPadding(new Insets(10, 10, 10, 10));

		// Setting the vertical and horizontal gaps between the columns
		root.setVgap(5);
		form.setVgap(5);
		root.setHgap(5);
		form.setHgap(5);

		// Setting the Grid alignment
		root.setAlignment(Pos.CENTER);
		form.setAlignment(Pos.CENTER);

		root.setPrefSize(950, 700);

		int rowCount = 0;

		form.addRow(rowCount++, clientChooserLabel, clientChoiceBox);

		form.addRow(rowCount++, clientIdLabel, clientIdTxt);

		form.addRow(rowCount++, clientSecretLabel, clientSecretTxt);

		form.addRow(rowCount++, redirectURILabel, redirectURITxt);

		form.addRow(rowCount++, hostLabel, hostTxt);

		form.addRow(rowCount++, audienceLabel, audienceTxt);

		form.addRow(rowCount++, authEndpointLabel, authEndpointTxt);

		form.addRow(rowCount++, tokenEndpointLabel, tokenEndpointTxt);

		form.addRow(rowCount++, scopeLabel, scopeTxt, submit);

		form.addRow(rowCount++, codeLabel, codeTxt, exchangeCode);

		root.add(form, 0, 0);
		root.add(browser, 0, 1, 3, 1);

		root.add(loggingArea, 4, 1, 1, 1);

		// oAuth2Client = new OAuth2ClientKeycloak(this);

		return root;

	}

	private void load(String uri) {
		webEngine.load(uri);
	}

	private ChangeListener<? super String> locationChangedListener() {
		return (observable, oldValue, newValue) -> {

			try {
				URL newURL = new URL(newValue);
				URL oldURL = new URL(oldValue);

				urlChanged(oldURL, newURL);

			} catch (MalformedURLException e) {

			}

		};
	}

	private EventHandler<ActionEvent> callExchange() {
		return (e) -> {

			logInfo("Exchange Code");
			OAuth2ClientData data = formDataToValueObj();

			logToken(oAuth2Client.exchangeCodeForToken(data, codeTxt.getText()));

		};
	}

	private EventHandler<ActionEvent> callAuthorize() {
		return (e) -> {

			try {
				URI callback = new URI(redirectURITxt.getText());
				urlStreamHandlerFactory.addScheme(callback.getScheme());
			} catch (URISyntaxException e1) {

			}

			URIBuilder builder = createAuthURI();

			load(builder.toString());

		};
	}

	private URIBuilder createAuthURI() {

		OAuth2ClientData data = formDataToValueObj();

		return oAuth2Client.createAuthURI(data);
	}

	private OAuth2ClientData formDataToValueObj() {
		OAuth2ClientData data = new OAuth2ClientData(clientIdTxt.getText(), //
				clientSecretTxt.getText(), //
				redirectURITxt.getText(), //
				hostTxt.getText(), //
				audienceTxt.getText(), //
				authEndpointTxt.getText(), //
				tokenEndpointTxt.getText(), scopeTxt.getText() //
		);
		return data;
	}

	@Override
	public void urlChanged(URL oldURL, URL newURL) {
		List<NameValuePair> query_pairs = null;

		logInfo("Location: " + newURL);

		query_pairs = URLEncodedUtils.parse(newURL.getQuery(), Charset.forName("utf-8"));

		for (NameValuePair item : query_pairs) {
			if (item.getName().equals("code")) {
				logInfo("Code: " + item.getValue());
				OAuth2ClientData data = formDataToValueObj();

				JWTAccessToken token = oAuth2Client.exchangeCodeForToken(data, item.getValue());
				logToken(token);

			}
		}
	}

	private void logToken(JWTAccessToken token) {

		logInfo("JWT Header : " + token.getHeader());
		logInfo("JWT Body : " + token.getBody());
	}

	@Override
	public void logError(String msg) {
		logMessage("[ERROR] " + msg);
	}

	@Override
	public void logException(Exception ex) {
		logError(ex.getMessage());
	}

	@Override
	public void logInfo(String msg) {
		logMessage("[INFO] " + msg);
	}

	private void logMessage(String msg) {
		loggingArea.appendText(msg + System.getProperty("line.separator"));
	}

}
