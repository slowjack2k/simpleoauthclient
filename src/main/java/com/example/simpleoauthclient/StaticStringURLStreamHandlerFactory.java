package com.example.simpleoauthclient;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashSet;
import java.util.Set;

public class StaticStringURLStreamHandlerFactory implements URLStreamHandlerFactory {
	private Set<String> schemes;

	public StaticStringURLStreamHandlerFactory() {
		super();
		this.schemes = new HashSet<>();
	}

	public void addScheme(String scheme) {
		if (scheme.startsWith("http")) {
			return;
		}

		schemes.add(scheme);
	}

	@Override
	public URLStreamHandler createURLStreamHandler(String protocol) {
		if (schemes.contains(protocol)) {
			return new StaticStringURLHandler();
		}
		return null;
	}
}
