package com.example.simpleoauthclient;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAccessToken {
	private String token;
	private String header;
	private String body;
	private String sig;

	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final Base64 BASE64_URL = new Base64(true);

	public JWTAccessToken(String token) {
		super();
		this.token = token;

		String parts[] = token.split("\\.");

		header = parts[0];
		body = parts[1];
		sig = parts[2];
	}

	public String getBody() {
		return base64ToJson(body);
	}

	public String getToken() {
		return token;
	}

	public String getHeader() {
		return base64ToJson(header);
	}

	public String getSig() {
		return sig;
	}

	private String base64ToJson(String bodyBase64) {
		String body = new String(BASE64_URL.decode(bodyBase64));
		Object json;
		try {
			json = MAPPER.readValue(body, Object.class);
			return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		} catch (IOException e) {
			return e.getMessage();
		}

	}
}
