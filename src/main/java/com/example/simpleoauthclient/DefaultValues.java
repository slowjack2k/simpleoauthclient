package com.example.simpleoauthclient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DefaultValues {
	private static final String CONFIG_DEFAULT = "config/default.properties";

	private static final Logger logger = LogManager.getLogger(DefaultValues.class);

	private Properties prop;

	public DefaultValues() {
		super();
		prop = new Properties();

		logger.info("Try to load " + CONFIG_DEFAULT);

		try (InputStream input = new FileInputStream(CONFIG_DEFAULT)) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			logger.warn(CONFIG_DEFAULT + " colud not be loaded");
		}
	}

	public String getClientId() {
		return prop.getProperty("clientId", "");
	}

	public String getScope() {
		return prop.getProperty("scope", "");
	}

	public String getClientSecret() {
		return prop.getProperty("clientSecret", "");

	}

	public String getAudience() {
		return prop.getProperty("audience", "");

	}

	public String getRedirectURI() {

		return prop.getProperty("redirectURI", "");
	}

	public String getHost() {
		return prop.getProperty("host", "");
	}

	public String getAuthEndpoint() {
		return prop.getProperty("authEndpoint", "");
	}

	public String getTokenEndpoint() {
		return prop.getProperty("tokenEndpoint", "");

	}

}
