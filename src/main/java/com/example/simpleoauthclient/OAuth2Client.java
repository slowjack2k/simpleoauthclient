package com.example.simpleoauthclient;

import org.apache.http.client.utils.URIBuilder;

public interface OAuth2Client {

	URIBuilder createAuthURI(OAuth2ClientData data);

	JWTAccessToken exchangeCodeForToken(OAuth2ClientData data, String code);

}